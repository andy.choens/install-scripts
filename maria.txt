Last metadata expiration check: 0:30:00 ago on Thu Mar 10 20:13:35 2022.
=============================================================== Name & Summary Matched: maria ================================================================
R-RMariaDB.x86_64 : Database Interface and 'MariaDB' Driver
holland-mariabackup.noarch : Holland plugin for Mariabackup
mariadb-backup.x86_64 : The mariabackup tool for physical online backups
mariadb-connect-engine.x86_64 : The CONNECT storage engine for MariaDB
mariadb-connector-c.i686 : The MariaDB Native Client library (C driver)
mariadb-connector-c.x86_64 : The MariaDB Native Client library (C driver)
mariadb-connector-c-devel.i686 : Development files for mariadb-connector-c
mariadb-connector-c-devel.x86_64 : Development files for mariadb-connector-c
mariadb-connector-c-test.x86_64 : Testsuite files for mariadb-connector-c
mariadb-connector-odbc.i686 : The MariaDB Native Client library (ODBC driver)
mariadb-connector-odbc.x86_64 : The MariaDB Native Client library (ODBC driver)
mariadb-devel.i686 : Files for development of MariaDB/MySQL applications
mariadb-devel.x86_64 : Files for development of MariaDB/MySQL applications
mariadb-embedded.i686 : MariaDB as an embeddable library
mariadb-embedded.x86_64 : MariaDB as an embeddable library
mariadb-embedded-devel.i686 : Development files for MariaDB as an embeddable library
mariadb-embedded-devel.x86_64 : Development files for MariaDB as an embeddable library
mariadb-java-client.noarch : Connects applications developed in Java to MariaDB and MySQL databases
mariadb-java-client-javadoc.noarch : Javadoc for mariadb-java-client
mariadb-oqgraph-engine.x86_64 : The Open Query GRAPH engine for MariaDB
mariadb-pam.x86_64 : PAM authentication plugin for the MariaDB server
mariadb-rocksdb-engine.x86_64 : The RocksDB storage engine for MariaDB
mariadb-s3-engine.x86_64 : The S3 storage engine for MariaDB
mariadb-server.x86_64 : The MariaDB server and related files
mariadb-server-utils.x86_64 : Non-essential server utilities for MariaDB/MySQL applications
mariadb-sphinx-engine.x86_64 : The Sphinx storage engine for MariaDB
mariadb-test.x86_64 : The test suite distributed with MariaDB
perl-DBD-MariaDB.x86_64 : MariaDB and MySQL driver for the Perl5 Database Interface (DBI)
perl-DBD-MariaDB-tests.x86_64 : Tests for perl-DBD-MariaDB
php-williamdes-mariadb-mysql-kbs.noarch : An index of the MariaDB and MySQL Knowledge bases
==================================================================== Name Matched: maria =====================================================================
mariadb.x86_64 : A very fast and robust SQL database server
mariadb-common.i686 : The shared files required by server and client
mariadb-common.x86_64 : The shared files required by server and client
mariadb-connector-c-config.noarch : Configuration files for packages that use /etc/my.cnf as a configuration file
mariadb-cracklib-password-check.x86_64 : The password strength checking plugin
mariadb-errmsg.i686 : The error messages files required by server and embedded
mariadb-errmsg.x86_64 : The error messages files required by server and embedded
mariadb-gssapi-server.x86_64 : GSSAPI authentication plugin for server
mariadb-server-galera.x86_64 : The configuration files and scripts for galera replication
=================================================================== Summary Matched: maria ===================================================================
anope-mysql.x86_64 : MariaDB/MySQL modules for Anope IRC services
mysql-selinux.noarch : SELinux policy modules for MySQL and MariaDB packages
percona-xtrabackup.x86_64 : Online backup for InnoDB/XtraDB in MySQL, Percona Server and MariaDB
phpMyAdmin.noarch : A web interface for MySQL and MariaDB
python3-mysqlclient.x86_64 : MySQL/mariaDB database connector for Python
