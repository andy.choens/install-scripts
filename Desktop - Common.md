# About

Tools & things installed across desktop environments.

```bash
dnf group list
```

TODO: Merge Desktop-Common and Common.

# Accessories

## Fonts

```bash
## Fonts I like:
sudo dnf install \
     adobe-source-code-pro-fonts \
     adobe-source-sans-pro-fonts \
     adobe-source-serif-pro-fonts \
     cascadia-fonts-all \
     fira-code-fonts \
     ibm-plex*
```

## Other

```bash
## Consider deleting if not used:
sudo dnf install mediawriter.x86_64
```



# Developer Tools - GLOBAL

Install these globally and not in a special environment.

## Build Tools

```bash
## I tend to install this system-wide:
sudo dnf group install "C Development Tools and Libraries"

## Only if needed:
sudo dnf install maven
```

## Command Line Tools

```bash
sudo dnf install \
    dos2unix \
    fd-find \
    htop \
    libtree-sitter.x86_64 \
    nnn \
    pandoc \
    pwgen \
    ranger \
    ripgrep \
    the_silver_searcher
```

## Data

```bash
## Definitely install these:
sudo dnf install \
    freetds freetds-devel.x86_64 \
    unixODBC.x86_64 unixODBC-devel.x86_64

## This is entirely optional:
sudo dnf install unixODBC-gui-qt.x86_64
```

## Editors

I just gotta have these installed:

```bash
sudo dnf install \
    emacs \
    kpcli \
    libtool \
    neovim neovim-ale neovim-qt python3-neovim 
```

Yes, getting emacs actually up and running _can be_ finicky. And I'm still
learning how to configure neovim.

I only install DBeaver on desktop/primary development systems:

```bash
flatpak install \
    io.dbeaver.DBeaverCommunity
```

Although written by Microsoft, this is still a good text editor:

```bash
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
dnf check-update
sudo dnf install code
```

## Git

```bash
sudo dnf install \
    git \
    git-credential-libsecret \
    git-crypt \
    git-gui \
    gitk \
    git-tools \
    tig
```

## Python

```bash
sudo dnf install black python3-jedi
```

# Developer Tools - Toolboxes

Install these globally and not in a special environment.

```bash
sudo dnf install toolbox
```


## Nodejs

```bash
toolbox create nodejs
sudo dnf install nodejs.x86_64 npm.x86_64
```

## Python

```bash
toolbox create ds-python
toolbox enter ds-python
sudo dnf install -y \
    python3-Cython.x86_64 \
    python3-nbconvert \
    python3-notebook \
    python3-numpy \
    python3-pandas \
    python3-pymssql \
    python3-qtconsole \
    python3-rope \
    python3-setuptools python3-setuptools_scm \
    python3-scipy \
    python3-statsmodels \
    python3-sqlalchemy \
    python3-wheel \
    visidata
    
##python3-venv \
##python3-spyder python3-spyder-kernels python3-pyls-spyder.noarch \
```

## R

This gives you a simple R setup.

```bash
toolbox create ds-r
toolbox enter ds-r
sudo dnf install \
     cairo-devel \
     libcurl-devel \
     libgit2.x86_64 libgit2-devel.x86_64 \
     libsodium.x86_64 libsodium-devel.x86_64 \
     openssl-devel \
     R R-devel \
     R-core.x86_64 R-core-devel.x86_64 \
     R-RMariaDB
     R-odbc \
     R-RSQLite \
     texlive-opensans.noarch \
     texlive-latexcolors.noarch
     visidata

## Useful if you want to use VSCode.
pip install radian
```

This is a larger, more complex install:

```bash
toolbox create ds-rstudio-desktop
toolbox enter ds-rstudio-desktop
sudo dnf install \
     cairo-devel \
     libcurl-devel \
     libgit2.x86_64 libgit2-devel.x86_64 \
     libsodium.x86_64 libsodium-devel.x86_64 \
     openssl-devel \
     R R-devel \
     R-core.x86_64 R-core-devel.x86_64 \
     R-RMariaDB
     R-odbc \
     R-RSQLite \
     visidata

sudo dnf install \
     rstudio-desktop.x86_64 \
     texlive-opensans.noarch \
     texlive-latexcolors.noarch
```

And, finally, a RStudio server install

```bash
toolbox create ds-rstudio-desktop
toolbox enter ds-rstudio-desktop
sudo dnf install \
     cairo-devel \
     libcurl-devel \
     libgit2.x86_64 libgit2-devel.x86_64 \
     libsodium.x86_64 libsodium-devel.x86_64 \
     openssl-devel \
     R R-devel \
     R-core.x86_64 R-core-devel.x86_64 \
     R-RMariaDB
     R-odbc \
     R-RSQLite \
     visidata

sudo dnf install \
     rstudio-server.x86_64 \
     texlive-opensans.noarch \
     texlive-latexcolors.noarch
```


Either way, you are gonna want to install some useful packages now. Your needs
may vary, but this is a good starting point. It will take a few minutes to
complete, but then you should be in good shape:

```r
## Be patient, this takes a while.
packages <- c(
    "DBI",
    "DT",
    "IRdisplay",
    "IRkernel",
    "RColorBrewer",
    "colorout",
    "config",
    "devtools",
    "flexdashboard",
    "modelr",
    "openxlsx",
    "plotly",
    "rmarkdown",
    "rsconnect",
    "shiny",
    "tidymodels",
    "tidyverse"
)
install.packages(packages, dependencies = TRUE)
```



# Graphics

```bash
sudo dnf install \
    darktable*
```

# Internet

```bash
sudo dnf install \
    remmina.x86_64 remmina-gnome-session.x86_64 \
    remmina-plugins-exec.x86_64 \
    remmina-plugins-nx.x86_64 \
    remmina-plugins-rdp.x86_64 \
    remmina-plugins-secret.x86_64 \
    remmina-plugins-spice.x86_64 \
    remmina-plugins-st.x86_64 \
    remmina-plugins-vnc.x86_64 \
    remmina-plugins-www.x86_64 \
    remmina-plugins-xdmcp.x86_64

firefox-wayland \
```

```bash
sudo rpm --import https://download.wavebox.app/static/wavebox_repo.key
sudo wget -P /etc/yum.repos.d/ https://download.wavebox.app/stable/linux/rpm/wavebox.repo
sudo dnf install Wavebox
```



# Office

```bash
sudo dnf install \
    hunspell-en-US.noarch hunspell-en.noarch \
    libreoffice.x86_64

flatpak install slack us.zoom.Zoom 
flatpak install com.microsoft.Teams
```



# Science & Engineering

```bash
toolbox create ds-julia
toolbox enter ds-julia
sudo dnf install \
    julia.x86_64 julia-devel.x86_64
```

TODO: Beef up my latex stuff.

```bash
sudo dnf install \
        
```

Apache Superset:

- This is all from their website.
- Should only have to install cyrys-sasl-devel and openldap-devel.

```bash
## Globally installed
    sudo dnf install \
        cyrus-sasl-devel \
        gcc gcc-c++ \
        libffi-devel \
        openldap-devel \
        python3-devel python3-pip python3-wheel \
        openssl-devel

## Prepare the virtual environment.
mkdir ~/venv
python3 -m venv ~/venv/superset
. venv/superset/bin/activate

# Install Apache Superset
pip install apache-superset

# Setup Apache Superset Database
superset db upgrade

# Create an admin user in your metadata database (use `admin` as username to be able to load the examples)
$ export FLASK_APP=superset
superset fab create-admin

# Load some data to play with
superset load_examples

# Create default roles and permissions
superset init

# To start a development web server on port 8088, use -p to bind to another port
superset run -p 8088 --with-threads --reload --debugger



```


# System Settings

- Map Caps Lock to Ctrl
- Make sure you get HiDPI squared away.
- Make dnf faster!
  - `sudo vim /etc/dnf/dnf.conf`
```
# append the following two lines to
# /etc/dnf/dnf.conf
fastestmirror=true
deltarpm=true
```
- `sudo dnf group upgrade --with-optional Multimedia`

## Resources
- https://en.opensuse.org/High_DPI

## Nvidia

If on a laptop, you should probably disable the Nvidia card to save the the 
battery.

```
sudo echo "blacklist nouveau" >> /etc/modprobe.d/nouveau.conf
```

## Firefox

I have multiple profiles:

- Andy (Personal) 
- Acuitas
- ACPHS

Each email address has an FireFox account. FF will then send you the code.
This will sync extensions, bookmarks, etc.

## Fixes!

- I have laptops with hipdi screens and external monitors which are
  not (SAD). If you are having weird scaling problems with Firefox,
  etc., open dconf-editor and look for xsettings -> overrides and make
  sure it is BLANK! If there's anything in it - remove it and your
  problems will probably go away.
- If you need to tweak the boot params (UEFI)
    - Edit: /etc/default/grub
    - Run: sudo grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg
    - Reboot: Enjoy!
