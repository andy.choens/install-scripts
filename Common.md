# About

Contains some steps I follow on nearly all systems.

# Upgrade

Before doing anything else, make sure you're full upgraded.

```bash
sudo dnf upgrade
```

And, better yet, if you're on a laptop or workstation, make sure you can listen
to some tunes.

```bash
sudo dnf group upgrade --with-optional Multimedia
```



# Git

Sections:
- Setup
- CLI Applications
- Power Management

## RSA

Makes it easier to manager GitHub/GitLab repos.

- Reference: https://help.github.com/articles/connecting-to-github-with-ssh/
- Copies the contents of the id_rsa.pub file to your clipboard.
- Give this to GitHub/GitLab, then proceed.

```bash
sudo apt install git xclip
ssh-keygen -t ed25519 -C "Andy from _"
ssh-add ~/.ssh/id_rsa
xclip -sel clip < ~/.ssh/id_rsa.pub
```

I put all of my projects into a consistent series of file folders. This lets me
control which "me" signs the git commits.

```bash
cd ~
mkdir dev
mkdir dev/acphs
mkdir dev/acuitas
mkdir dev/andy
cd dev/andy
git clone https://gitlab.com/Choens/install-scripts.git
```

## Git Config

- Create a file called ~/.gitconfig
- Add the following:

```
[includeIf "gitdir:dev/acphs/"]
  path = .gitconfig-acphs
[includeIf "gitdir:dev/acuitas/"]
  path = .gitconfig-acuitas 
[includeIf "gitdir:dev/andy/"]
  path = .gitconfig-andy
[includeIf "gitdir:.emacs.d/"]
  path = .gitconfig-emacs
[includeIf "gitdir:Notes/"]
  path = .gitconfig-notes
```

The create the following files:

- .gitconfig-acphs
- .gitconfig-acuitas
- .gitconfig-andy
- .gitconfig-emacs
- .gitconfig-notes

Complete the contents as appropriate:

```
[user]
    name = Your Name
    email = your.name@gmail.com
```


# CLI Applications

Some common/useful CLI tools.

```bash
sudo dnf install \
    aspell-en \
    hunspell-en \
    hwinfo \
    htop \
    p7zip p7zip-plugins \
    pwgen \
    ranger \
    xclip
```

# Power Management

I am going to experiment with NOT installing powertop on my laptop.

```bash
# TODO: Research
sudo dnf install \
     powertop

##sudo dnf install \
##     tlp

# Test/Enable TLP
# https://wiki.archlinux.org/index.php/TLP
# TODO: Skipped in order to not get stuck.
##sudo systemctl start tlp.service  
#sudo systemctl start tlp-sleep.service

##sudo systemctl enable tlp.service  
#sudo systemctl enable tlp-sleep.service

# Settings to review
# /etc/default/tlp
```



# Meraki Cisco

Note: You only need to do this if you need to interact with a Meraki VPN.

```bash
## Neither of these will work with Meraki, so make sure they aren't there.
sudo dnf remove libreswan openswan
```

``` bash
## Then, install these packages:
sudo dnf install \
    NetworkManager-l2tp\
    NetworkManager-strongswan.x86_64 \
    strongswan
```

```bash
## And stop xl2tpd, because it messes things up.
sudo systemctl stop xl2tpd
```

```bash
## I should be able to eventually remove this.
## sudo dnf remove plasma-nm-l2tp.x86_64 plasma-nm-strongswan \
```

# Flatpak

```bash
# If this works, skip the next part.
# Browse to: https://flatpak.org/setup/Fedora/ and download the Flathub
# repository file, or intsall it this way:
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

## Useful package if you're gonna use flatpaks, but likely already installed:
sudo dnf install xdg-desktop-portal.x86_64
```

# Nvidia Driver

```bash
## NVidia Driver
## This is only needed on systems with an NVIDIA card.
## And even then, I don't often install it. I usually just blacklist the nouveau
## driver and use the built-in Intel video card.
sudo dnf install kmod-nvidia
```

# Resources

- [HiDPI](https://www.linuxsecrets.com/archlinux-wiki/wiki.archlinux.org/index.php/HiDPI.html)
- [Configuring Meraki Client VPN on Linux Mint 19 ](https://stuffjasondoes.com/2018/08/16/configuring-meraki-client-vpn-on-linux-mint-19-network-manager/)
- https://bugzilla.redhat.com/show_bug.cgi?id=1807024
- https://codepre.com/how-to-install-gdm-sddm-lightdm-display-manager-on-fedora.html
- https://dnf.readthedocs.io/en/latest/command_ref.html
- https://bugzilla.redhat.com/show_bug.cgi?id=1807024
- https://stuffjasondoes.com/2018/08/16/configuring-meraki-client-vpn-on-linux-mint-19-network-manager/
